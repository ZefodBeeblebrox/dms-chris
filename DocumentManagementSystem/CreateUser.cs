﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManagementSystem
{
    public partial class CreateUser : Form
    {
        public CreateUser()
        {
            InitializeComponent();
        }

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCreateUsername.Text) || string.IsNullOrWhiteSpace(txtCreatePassword.Text) ||
                string.IsNullOrWhiteSpace(txtCreateForename.Text) || string.IsNullOrWhiteSpace(txtCreateSurname.Text) ||
                (rBtnAdmin.Checked == false && rBtnAuthor.Checked == false && rBtnDistr.Checked == false))
            {
                MessageBox.Show("Some options were left blank");
                return;
            }
            else
            {
                if (txtCreatePassword.Text.Length < 8)
                    MessageBox.Show("Password should be at least 8 characters long.");
                else
                {
                    DialogResult dr = MessageBox.Show("Are you sure you want to create user?", "Sure", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        string role;

                        if (rBtnAuthor.Checked)
                            role = "0";
                        else if (rBtnDistr.Checked)
                            role = "1";
                        else if (rBtnAdmin.Checked)
                            role = "2";
                        else
                        {
                            return;
                        }
                        string username = txtCreateUsername.Text;
                        string password = txtCreatePassword.Text;
                        string forename = txtCreateForename.Text;
                        string surname = txtCreateSurname.Text;

                        sqlWorkBench.createUser(username, password, forename, surname, role);

                        MessageBox.Show("User has been created.");

                        txtCreateForename.Text = string.Empty;
                        txtCreateSurname.Text = string.Empty;
                        txtCreateUsername.Text = string.Empty;
                        txtCreatePassword.Text = string.Empty;

                        rBtnAdmin.Checked = false;
                        rBtnAuthor.Checked = false;
                        rBtnDistr.Checked = false;
                    }
                }
            }
        }

        private void btnCancelCreate_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
